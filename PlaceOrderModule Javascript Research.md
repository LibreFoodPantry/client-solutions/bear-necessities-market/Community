## What is the goal or the problem, and why is it important?

The goal is to understand how to convert the JavaScript in the PlaceOrderModule into YAML, so that refactoring of the PlaceOrderModule can take place.

## How are you going to achieve the goal or solve the problem?
By examining the Javascript in the PlaceOrderModule and devising a list of calls that were present and seeing what concepts or similar practices in YAML could be extracted.

## What will be produced as a result of this effort and where will it live on GitLab?

A list of resources and examples of the Javascript present in the Backend of PlaceOrderModule will be placed in the outcome section of this issue.

## Give a justification of weight or due date.

A weight of 1 will be assigned since this will not need any more time to be accomplished. 

## Who will do it, and what will they do?

@zx83921 will take on this task and research JavaScript. 

## What was the outcome?
Due to the limited number of calls present in the Backend of PlaceOrderModule, only two elements were found to be relevant and searchable. The results of the research can be found in [this GitLab wiki](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/ordersystem/placeorder/api/-/wikis/Sprint_One_Issue_23_Results).

<div>

# <span dir="">Functions found in the PlaceOrderModule </span>

</div>
<div>

## <span dir="">Backend/routes/order.js </span>

</div><span dir="">Require </span>

<div>

* <span dir="">Resources: </span>
  * [<span dir="">Documentation</span>](https://nodejs.org/api/modules.html#requireid)<span dir=""> </span>
* <span dir="">This is a part of NodeJS meant to tell NodeJS what module to import. It can also be used to import JSON and local files. In the PlaceOrderModule its used just to import the Express Module.</span>
* <span dir="">Example Syntax from documentation: const myLocalModule = require('./path/myLocalModule');</span>
* <span dir="">File in PlaceOrderModule: /routes/order.js </span>
* <span dir="">Attributes found within the PlaceOrderModule: </span>
  * **<span dir="">id</span>** 
  * **<span dir="">Type</span>**<span dir="">: String </span>
  * <span dir="">A module **name** </span>
* <span dir="">How to address in YAML: Given the call is used to import the Express module, which will not be used in REACT API as it is a JSON module, there is no need to implement this into our refactored code.</span>

</div><span dir="">Values used in PlaceOrderModule </span>

* <span dir="">id: “express” </span>
  * <span dir="">Tells NodeJS to use the Express Module </span>

### <span dir="">Express Module </span>
<div>

* <span dir="">Resources: </span>
  * [<span dir="">Express Documentation</span>](https://expressjs.com/en/5x/api.html)<span dir=""> </span>

</div>
<div>

* <span dir=""Express is a JSON module designed to handle HTTP requests bodies.</span>
* <span dir="">File in PlaceOrderModule: /routes/order.js </span>
* <span dir="">Attributes found within PlaceOrderModule: </span>
  * <span dir="">Router – This is a class that controls the HTTP requests like GET, POST, and PUT. It can be referenced as a variable.</span>
* <span dir="">How to address in YAML: The functions derived using Router in PlaceOrderModule are the HTTP requests that must be located in the index.yaml in the paths directory of the refactored code. The functions, such as GET ALL Orders and POST Order, would need to be created as separate YAML files in the paths directory that must be referenced within the paths/index.yaml file. </span>

</div>