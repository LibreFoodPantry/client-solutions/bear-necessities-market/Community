Research links:

- Introduction to SemVer https://blog.greenkeeper.io/introduction-to-semver-d272990c44f2
- Why versioning is important https://betterprogramming.pub/why-versioning-is-important-and-how-to-do-it-686ce13b854f 
- semantic-release tool GitHub https://github.com/semantic-release/semantic-release
- semantic-release documentation https://semantic-release.gitbook.io/semantic-release/usage/getting-started
- Software Upgrade Versioning: SemVer https://medium.com/@amirm.lavasani/software-upgrade-versioning-semantic-versioning-4359cab321d4

### Semantic Versioning:

- A common frustration for developers is something known as **Dependency hell**
- Dependency hell is comprised of **version lock** where dependencies are too tight. This means that a package cannot be upgraded further unless you release an update of *every single package*. There's also **version promiscuity** where dependencies are too loose. This means that compatibility has been provided *for more versions than necessary*, causing issues later in the application's life. Semantic Versioning prevents this frustration.
- Semantic Versioning is a developing practice that provides version numbers to an application. The benefits include:
  - Showcasing progress made for an application.
  - Providing compatibility between different versions of the same software.
  - Generating user interest via new features.
- As specified by [SemVer](https://semver.org), Semantic Versioning must follow a familiar format of 3 integers: `Major` release, `Minor` release, and `Patch` release (X.X.X). Optionally, a pre-release label can be tagged on after the patch release (-beta, -alpha, etc.) 
  - Interestingly enough, SemVer has an issue on their [GitHub Page](https://github.com/semver/semver/issues/411#issuecomment-346446936) of changing `Major` and `Minor` to `Breaking` and `Feature` in an attempt to 'make things much more clear'. Others also opt to change `Patch` to `Fix`.
- A Major change is required when a developer makes incompatible API changes. It is not safe to update to new Major versions unless work has been put in to make sure the application retains compatibility.
- A Minor change is required when you implement new backwards-compatible functionality. It is safe to update when Minor changes have been made.
- A Patch is required when bug fixes are made to existing features. It is also safe to update when Patches have been made.

### Semantic Release (A Semantic Versioning tool for GitHub projects):

- [Semantic release](https://github.com/semantic-release/semantic-release) is a tool that automates the decision-making on a software product’s build version by utilizing GitHub’s commit messages.
- By default, Semantic Release utilizes [Angular Commit Message Conventions](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-format) to determine what changes are Major, Minor, and are Patches (These are fix(), feat(), and perf() respectively).
- Semantic release is meant to be automated with CI after every successful build on the release branch.
- One can get Semantic Versioning up and running on their GitHub project by installing it within their project (preferably in its own directory) and configuring the project’s CI/CD service to run semantic-release.
