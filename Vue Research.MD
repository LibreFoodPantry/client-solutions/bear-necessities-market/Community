# Vue

## Vue Project Reference from LibreFoodPantry
- The best available example for how the BNM system is utilizing vue is provided 
in the LibreFoodPantry project at this link : https://gitlab.com/LibreFoodPantry/training/microservices/microservices-examples/frontend. This is an axample of a microservices FrontEnd module that utilizes vue and it is a good starting point to look at since the files are relatively small and uncomplicated.

## Vue Docs and helpful links
- From there, the second best reference is the Vue docs on their website located here: https://vuejs.org/v2/guide/. This site has the different capabilities of vue listed in sepearte sections on the left hand side menu. Each section provides mini tutorials which can be helpful in grasping the information. A good place to start however, would be the "Getting Started" section located under Introduction in the menu.
    - This reference has about everything you need to start utilizing vue. Once you feel comfortable with some of the syntax, a good place to try things out can be found here: https://vuejs.org/tutorial/). This site provides a window to try out statements and it allows you to toggle between index.html and a .vue to compare the two. It also alows you to preview the compiled code in real time.
- Additionally, here is a three part series that goes through how to convert a project from REACT to Vue : https://dan-foley.medium.com. It covers topics such as syntax differnces, common Vue Add Ons and tools, and overall benefits.
