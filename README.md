# Logistics

This project holds information about how to approach developing Bear Necessities Market. 

## Architecture

BNM will follow the REST architecture implemented over HTTP/HTTPS using JSON representation. OpenAPI (YAML) will be used for specifying REST APIs.

## Implementation
* [Stoney's Explanation of Docker Example](https://echo360.org/media/114ccaee-25c3-457c-901e-a26736707662/public)
* [Stoney's Notes on Docker Example](https://docs.google.com/document/d/1JcZ7K-pkqsnBYDzf-nmLc8M88MNFBWrDIfkrwJ1ulx4/edit)
