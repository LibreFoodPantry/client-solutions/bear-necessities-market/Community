﻿# Vue Research - (API Oriented Development)
**Vue** operates along two main features:
- **Declarative rendering:** Vue extends standard HTML with a template syntax that allows us to declaratively describe HTML output based on JavaScript state.
- **Reactivity:** Vue automatically tracks JavaScript state changes and efficiently updates the DOM when changes happen.

**Vue** offers a flexible framework to ensure  proper compatability across the web.

### Vue Component Structure

**Single-File Components** (AKA **.vue** files): is an encapsulation of everything that Vue needs to start building.
- SFC files contain the **logic** (JavaScript), **template** (HTML), and **style** (CSS) within a single file.
- Each file created, is to be written in **SFC Format**, this is the recomended way to author **Vue components**.

### API

Two different **API styles** are able to be utilized when establishing Vue components.

- **Options API**
- **Composition API**

Through **Options API**
- **Component logic** is defined and created through the object options:
- `data`
	- Properties returned from data() becomes reactive state and will be exposed on `this`
-  `methods`
	- Methods are **functions** that mutate state and trigger updates.
- and `mounted`
	- Lifecycle hooks are called at different stages of a component's lifecycle. This function will be **called** **when** the component is **mounted**.

**Properties** that are defined within these objects can be **accessed** by using `this` inside functions (points to an instance of specified component).
- Example: `{this.count}` will **return** the instance of the **count variable**, found within the declared `data` object.

Through **Composition API**
- Component logic will be established through **imported API functions**
	- The `setup` attribute allows us to use the Composition API with less boilerplate
	- **Top-level variables** and **functions** are declared within the `<script setup>`

### Axios - Auto Building Extension for API
- **Axios** is an extension used by **Vue** to automatically consume and display data from an API
- This is what we are using within the **BNM Project** as it easily allows us to specify and use the data declared by the **particular API endpoints.**
	- To properly utilize this extension
		- We will create a `data ()` property that will eventually house all of our information that is **imported from the API**
	- The data will then be **retrieved and assigned** by using the `mounted` **lifecycle hook**

### Helpful links
- Axios Documentation -  [How to consume an API](https://v2.vuejs.org/v2/cookbook/using-axios-to-consume-apis.html?redirect=true)
- Web-based [test IDE](https://vuejs.org/tutorial/#step-1) for Vue
- Vue documentation - [good overview of Vue processes](https://vuejs.org/guide/introduction.html#what-is-vue)
