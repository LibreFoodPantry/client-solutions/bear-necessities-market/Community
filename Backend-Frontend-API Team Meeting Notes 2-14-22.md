### Overview

- API calls needed: Adding in an item, updating an item, deleting an item, adding an ORDER, updating an Order, Deleting an order, Getting an orders contents, get orders by date

- Possible API calls to remove: get by email, maybe email verified since you need to be verified just to make an order

- Compared the old PlaceOrder module to the microservices examples

- All teams should meet with Stoney to get a greater idea as to the scope of the project and what needs to be done as there is confusion among all teams as to the direction of the PlaceOrder project

- API calls should have matching .js files located in src/endpoints(to be created) of the backend project.             
    
### Next Steps

- Get last semester requirements documentation for each team and place them in planning

#### API
- Meet with Stoney
- Discuss what API calls to keep

#### Frontend
- Meet with Stoney (Discuss custom orders)
- When converting from React to Vue look at microservices example

#### Backend
- Meet with Stoney about the actual structure of the database.
- Model the new Backend project off the microservices example
- Inventory database holds items - Backend placeorder holds orders
- redefine what an order should actually contain. 

    OrderID,
    Itemsordered, 
    dietary restrictions, 
    comments/preferences
    OrderStatus(maybe)
    Email(maybe), Email verification(probably not needed), dateOrdered(Likely needed), order type(Remove))


Links

https://gitlab.com/LibreFoodPantry/training/microservices/microservices-examples/backend
https://gitlab.com/LibreFoodPantry/training/microservices/microservices-examples/api 