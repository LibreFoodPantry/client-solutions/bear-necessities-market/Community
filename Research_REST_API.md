﻿# **<span dir="">REST API</span>**<span dir=""> </span>- [ISSUE 3](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/ordersystem/placeorder/api/-/issues/3)

## **<span dir="">Resources:</span>**<span dir=""> </span>

* [**<span dir="">Documentation for REST API</span>**](https://restfulapi.net/)<span dir=""> </span>

## **<span dir="">About REST API:</span>**<span dir=""> </span>

* <span dir="">An API is an interface allowing a frontend to communicate with a database without having direct access to the data by sending out commands. </span>
* <span dir="">Allows for the **front end** of the project, to communicate with the **backend** </span>
  * <span dir="">Requests go through the API using specific HTTP calls. </span>
  * <span dir="">The requested data, from the specified endpoint, is returned to the frontend, from the backend data. </span>
  * <span dir="">Upon the creation of a request, a piece of response code is sent by the server to the requester updating them on the status of the HTTP request. </span>

## **<span dir="">Requests consist of:</span>**<span dir=""> </span>

* <span dir="">Endpoints - URL being requested </span>
* <span dir="">Methods – The operation that has to be carried out on a piece of data (e.g. POST, GET, DELETE, etc.) </span>
* <span dir="">Headers – The body of a request that contains the data to be sent </span>
* <span dir="">Data – The values being sent to the server </span>
* <span dir="">Root-Endpoint - starting point of the API that you’re requesting from </span>

## **<span dir="">Basic calls/methods</span>**<span dir=""> </span>

* <span dir="">POST **–** Send a new piece of data to be added to a database </span>
* <span dir="">GET **–** Receive data from a database </span>
* <span dir="">DELETE **–** Delete a piece of data from a database </span>
* <span dir="">PUT - Update/Replace an entry of data </span>
* <span dir="">PATCH - Update/Modify an entry of data </span>
* <span dir="">PUT v. PATCH </span>
  * <span dir="">PUT </span>
    * <span dir="">Uses request URL  as an identifier to supply a modified version of the requested resource </span>
    * <span dir="">Which replaces the original version of the source </span>
  * <span dir="">PATCH </span>
    * <span dir="">Supplies set of instructions to modify the resource </span>

## **<span dir="">HTTP Request Responses</span>**<span dir=""> </span>

* **<span dir="">200</span>**<span dir=""> _OK_ – The request was accepted and the response contains the expected data </span>
* **<span dir="">201</span>**<span dir=""> _CREATED_ - Resource was created </span>
* **<span dir="">204</span>**<span dir=""> _NO CONTENT_ - The request was accepted but there was nothing to return </span>
* **<span dir="">400</span>**<span dir=""> BAD REQUEST - The data given was malformed </span>
* **<span dir="">404</span>**<span dir=""> _NOT FOUND_ – The requested resource does not exist </span>
* **<span dir="">405</span>**<span dir=""> _METHOD NOT ALLOWED_ – The requested method cannot be performed against the resource </span>
* **<span dir="">500</span>** <span dir="">SERVER ERROR – There was a problem with the internal server </span>

<span dir=""> </span>

# **<span dir="">Data Format / Data Serialization</span>**<span dir=""> </span>

## **<span dir="">YAML - Serialization/configuration files</span>**<span dir=""> </span>

* **<span dir="">Resources:</span>**<span dir=""> </span>
  * [**<span dir="">JSON Quick Facts</span>**](https://www.json2yaml.com/yaml-vs-json#:\~:text=JSON%20vs%20YAML,JSON%20with%20a%20YAML%20parser.)<span dir=""> </span>
  * [**<span dir="">Differences between YAML and JSON</span>**](https://stackoverflow.com/questions/1726802/what-is-the-difference-between-yaml-and-json)<span dir=""> </span>
  * [**<span dir="">In-Depth Look Into JSON and YAML</span>**](https://www.imaginarycloud.com/blog/yaml-vs-json-what-is-the-difference/#differences)<span dir=""> </span>
  * [**<span dir="">YAML Working on JS</span>**](https://gist.github.com/crrobinson14/8290174)<span dir=""> </span>

### **<span dir="">YAML Data Types</span>**<span dir=""> </span>

* <span dir="">Strings </span>
* <span dir="">Numbers </span>
* <span dir="">Boolean </span>
* <span dir="">Dates and timestamps (HUGE) </span>
* <span dir="">Sequences </span>
* <span dir="">Nested values </span>
* <span dir="">Null values </span>

### **<span dir="">JSON is a subset of YAML</span>**<span dir=""> </span>

* <span dir="">Lists begin with a hyphen </span>
* <span dir="">Dependent on whitespace/indentation </span>
* <span dir="">Better suited for configuration over JSON </span>
* <span dir="">Natively supports object references and relational trees </span>
* <span dir="">Allows for complex data types/structures </span>
  * <span dir="">This does slow down parsing a bit </span>
  * <span dir="">JSON wins, but we also get more types </span>
* <span dir="">Fewer libraries for support, the community is smaller than JSON </span>
  * <span dir="">JSON wins </span>

### <span dir="">With YAML, you can: </span>

* <span dir="">Self-reference </span>
* <span dir="">Support complex data types </span>
* <span dir="">Embedded block literals </span>
* <span dir="">YAML can also be parsed with a JSON parser </span>
  * **<span dir="">VALID YAML</span>** <span dir="">files **CAN** contain JSON objects  </span>
* <span dir="">YAML is also more easily legible for humans than JSON </span>

## **<span dir="">JSON - JavaScript Object Notation</span>**<span dir=""> </span>

* <span dir="">Works well alongside **MongoDB** and **Kubernetes**  </span>
* <span dir="">JSON **Data Types** </span>
  * <span dir="">Strings </span>
  * <span dir="">Numbers </span>
  * <span dir="">Objects </span>
  * <span dir="">Arrays </span>
  * <span dir="">Boolean </span>
  * <span dir="">Null values </span>
* **<span dir="">Root node</span>** 
  * <span dir="">Must be an array or an object </span>
* **<span dir="">Data</span>** 
  * <span dir="">Separated by commas </span>
  * <span dir="">Name/value pairs - with quotes </span>
* **<span dir="">Objects</span>** 
  * <span dir="">Limited between curly braces </span>
  * <span dir="">Can have array objects to create lists </span>
* **Arrays/lists**
  * Inside square brackets
  * <span dir="">Array attributes are separated by commas</span>
  * Array example in JSON below:<span dir=""> </span>
* <span dir="">![](https://lh3.googleusercontent.com/J054HWrAPSzXmyKJEvs1WUZMbRMt2DpaUtuaHin2AaqyYeI6xcawsJQUcZSb4Ng3Y_wk-KBeheVZpn4A-SdiU1YsxgAbD_ISddGyj5hAO929w5j0Q_uhK87eLADaeYqaELRNqLpY) </span>
* **<span dir="">Cons</span>**<span dir=""> </span>
  * <span dir="">Limited data types </span>
    * <span dir="">Only strings, numbers, JSON Objects, arrays, booleans, and nulls </span>
  * <span dir="">Doesn’t accept namespace, comment, or attributes </span>
  * Doesn't <span dir="">support complex configurations because JSON is quite simple</span>

## So, YAML or JSON?

### In terms of the scope of our project and goals,

* It makes sense to use **YAML**. I will tell you why...
* **YAML** allows for complex **data types**
  * As well as **objects** that will better help when trying to create a complex system, from the ground up.
* **YAML** is also more **adaptable**
  * Especially alongside **complex configurations** such as ours as everything is being currently maintained and developed
  * Continuous adaptations and configurations are important.
