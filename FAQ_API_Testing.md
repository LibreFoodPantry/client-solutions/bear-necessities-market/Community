﻿


# FAQ: API Testing | API Team Perspective
#### This list of questions stems from: [Issue - 32](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/ordersystem/placeorder/api/-/issues/32)
> ### What types of tests are needed for the MVP?

 - **Unit tests** 
	 - Are all that the mvp of our projects scope requires at this point
 - Our job is to **establish endpoint-validation**
 - And confirm that the endpoints can be called appropriately
	 - We do not need to worry about the following according to our scope:
	 - Integration tests
	 - Acceptance tests    
	 - End-to-end tests   
	 - Performance tests

> ### Where can we find OpenAPI tools to help with testing?

 -   **[https://openapi.tools/](https://openapi.tools/)**

> ### How are we to automate our testing?
 -   **[TCases - Tool to help build](https://github.com/Cornutum/tcases)**
	 - **Automates the test building process** by using openapi specification file
	 -  Test suites can be built by analysis of our **paths** and **schemas** automatically 
		 - This helps ensure that we catch small issues such as data requirements
	 -   Generates the blank test cases based off all possible tests that could exist
 -   Determining if a specification is *correct* is hard
	 - Syntax and everything must be checked for correctness
	 -  Therefore **you cannot automate** it, yet you can automate the building process
> ### Where can we see different testing tools in action?

 -   [Spikeathons](https://gitlab.com/LibreFoodPantry/training/spikeathons) found within Libre Food Pantry's Repository
	 - **Show different tools and methods** being used to test example code
	 - We must use our judgement to see what works best in our case

> ### How do we package and ship our API to the other teams?

 -   Once a new specification is created, it must be bundled
	 - This is done by running *npm bundle*  
 - Then, we must send the bundled file out to the other teams
	 - Alternatively, we can also use the **container registry** on GitLab

> ### How do we include the tools into our containerized development environment?

 -   Any test framework used, must go into our **dev dependency** 
	 - Inside package.json file under dev dependency
	- This will allow us to **containerize the dev environment** and the tools being used 

> ### What does the testing framework/process look like to the Frontend repository?

 -   **[Postman](https://www.postman.com/product/tools/)** can be used down the line for testing the api on the Frontend
	 - This tool however is focused on manual testing
 -   **[Insomnia](https://insomnia.rest/)** REST Client
	 -  **Works well with Frontend api testing**  
	 - This tool can be seen in action in [Spikeathons](https://gitlab.com/LibreFoodPantry/training/spikeathons), found within Libre Food Pantry's Repository

> ###  What does the testing framework/process look like to the Backend repository?

 -  The Backend is to **handle all tests**, aside from unit testing, we only need to worry about functionality testing of the endpoints
 - For extensions that could help the Backend further:
	 - **[Express-openapi-validator](https://github.com/cdimascio/express-openapi-validator)** can **AUTOMATICALLY** validate the OpenAPI Specification for the backend
	 -   **[Remote containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)** and REST Client
		 -   Allows us to write in a simple text file, a sample HTTP Request
	 -   **Chai** and **Mocha** are still to be used for backend automatic testing (current)
		 -   Testrunner, runs entrypoint on Mocha, bin script has the test script
		 -   Add version to Lib
