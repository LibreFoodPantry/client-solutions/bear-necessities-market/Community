# Connection for: InventorySystem to placeOrder

## GOAL: PlaceOrder Frontend should tell PlaceOrder API to pull inventory data from Inventory System’s backend

## Tools to consider: 
- [Sidekiq](https://sidekiq.org/products/enterprise.html)

## PlaceOrder Required Data:
- ItemID
- ItemName
- ItemQuantity

## InventorySystem Required Data:
- itemID
- itemName
- itemExpDate
- itemQuantity
- itemResQuantity 
(Maybe): it would be helpful for the Placeorder Backend to have each order include an attribute called ‘Reserved Quantity’, so that if an order is canceled the Reserved Quantity can be added back into the original Quantity.

Ensure that logic exists to test the mongoID found in the placeOrder API calls, against the: Inventory System API and the ItemQuantity variable
This will help ensure that no overflow of items are ordered

Remove any instance of item data except itemName and itemID from the placeOrder database and move it to the inventorySystem database. 
