- Yarn is a Javascript package manager developed as an improvement to NPM. It essentially makes add-ons very easy to implement, while doubling as a project manager as well. This will allow us to not only add Javascript-based widgets but compile and run the environment all under the same hood.
- Anything you can install with NPM, you can also install with Yarn.
- Migrating from NPM to Yarn is fairly straightforward, and Yarn allows for the node_modules folder to be used, preserving all 
  already installed packages.


How is it different than NPM?
- Yarn is much faster than NPM 4 and still slightly faster than NPM 5.
- Yarn is easier to use, you don't have to save after installing packages.
	
Basic Commands:

**yarn help** Accesses the full list of commands.

**yarn init** Starts a project.

**yarn add [package name]** Adds a package to your package folder.

**yarn remove [package name]** Removes a package from your package folder.

Executing <br>
**yarn** <br>
**yarn install** will install all packages in the folder.

All of this information was provided by the Yarn documentation. `https://yarnpkg.com/getting-started`